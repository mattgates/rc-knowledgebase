# An Introduction To RunCloud

RunCloud was founded by a Malaysian team of three PHP developers who were frustrated with the painful time consuming process of having to start from scratch every time they set up a new server. Their background means they welcome feedback from PHP developers, and the needs of the development community are foremost in their consideration when evolving the platform to further simplify the process of managing a server. PHP developers already have enough to worry about; the last thing they should have to worry about are the mundane tasks associated with server administration, including installing the latest software and security updates.

RunCloud takes the place of the traditional control panel by allowing you to install applications on your server, it’s features provide both easy access to information about your websites and the server, and the tools to manage them. For non-technical people, RunCloud is a server management tool that helps you run your websites and servers.

In our own tests, setting up a RunCloud server from scratch, including installing Ubuntu, Nginx, Apache, PHP, MySQL or MariaDB, and WordPress can take anywhere from 20 minutes to an hour. This does not take into consideration optimization or any security that may be installed on the server. With RunCloud and a VPS pre-installed with Ubuntu, setup takes about 10 minutes and optimizes and secures the entire server.

RunCloud doesn’t just allow you to run one website, but it can help you run as many websites as your VPS can support. Those websites are yours alone, no longer sharing resources with noisy neighbors. RunCloud employs an attractive Graphical User Interface (GUI), and with just a few clicks, your website will be up and running in no time!

With RunCloud, there is no more guessing or emailing support about why your website is running slow, as everything becomes clear from checking the Dashboard. The Dashboard is one of RunCloud’s most exceptional features and a primary reason most users choose to remain with the service. Clicking around, exploring, and getting a sense of how your websites are doing is why the RunCloud Dashboard is so appealing.

If you are ready to take control of your own server like a professional website administrator, get started today using RunCloud!