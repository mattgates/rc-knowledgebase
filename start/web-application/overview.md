# WEB APPLICATION


The word “application” or “web app” is synonymous with “website”. Click on Create Application.

WEB APPLICATION NAME: Give your “web application” (or website) a name. I’ve typed in myapp.

DOMAIN NAME: Type in a domain (you do not need a domain right away), but for the purposes of this, I’ve typed in myapp.com.

USER (OWNER OF THIS WEB APPLICATION): The user can be whoever you wish, as you can have multiple users, managed in the tab of SYSTEM USER. 

PHP VERSION: it is always recommended that you choose the latest stable version, which is currently PHP 7.2.

WEB APPLICATION STACK: 
NGINX + Apache2 Hybrid (You will be able to use .htaccess)
Native NGINX (You won’t be able to use .htaccess but it is faster)
It is recommended that you choose NGINX + Apache2 Hybrid, as many applications, especially if you plan to use WordPress, are going to need to utilize the .htaccess file.

STACK MODE: Production or Development are your two options. When first builidng the site, it is probably best to leave it in Development mode, while later you can change this to Production mode.

ADVANCED SETTINGS: Everything technically comes out of the box, so unless you know what you’re doing, just click on Add Web Application.

Within Web Application, a series of other menu options appear that are separate of the main menu. This is accessed by clicking into your web app.