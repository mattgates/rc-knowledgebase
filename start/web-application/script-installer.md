# What Is The Script Installer


A great feature of RunCloud is the Script Installer. 

Most scripts install within minutes, but larger scripts can take up to 20 minutes to download and install. 

Common scripts include:

* Concrete5
* Drupal
* Grav Core
* Joomla
* MyBB
* phpBB
* phpMyAdmin (database only)
* Matomoa (Piwik)
* PrestaShop
* WordPress


After these scripts are installed, navigate to your website where you can then proceed the normal setup.
