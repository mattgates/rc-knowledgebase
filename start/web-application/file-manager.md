# What Is The File Manager

RunCloud has a very unique and intuitive file management system that is straightforward. 

Within the file manager, you have the ability to create new files, new folders, change permissions, rename files, view and edit files, and delete files.

We will name our first file index.html since Apache/Nginx usually look for index.html or index.php.

The blank file will be created.

Click on it and use the menu above it to perform an action. In this case, we want to edit the file:

And finally, we have our output right when I type into the browser the IP address of the server, which hasn’t yet been assigned a domain.
