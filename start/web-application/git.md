# How To Use Git Respositories

A great thing about RunCloud is that you can attach GIt Repositories just by copying the URL of the repository and adding it. 

You will need a Git Deployment Key, but the beauty of this wonderful feature is that you can work right from Github and your changes will be synced within 2 minutes to your server.

Providers include: Custom Git Server, Bitbucket, Github, Gitlab, and a Self-Hosted Gitlab.