# Managing A Server With RunCloud

Most web designers and developers come for the service, but stay for the user-interface. One thing that often makes or breaks any platform is the dashboard and its ease of use. Sometimes platforms are ugly, thrown together, and made to work for whoever signs up and uses it, and are quick to get over whatever it looks like, in order to benefit from its functionality. 

This area, just before you reach the RunCloud Dashboard, is where you can receive a brief overview of your server status including the server’s name, the IP address, the host of your server (optional), disk usage, and memory usage. 

You can connect new servers, just one or unlimited depending on your plan, and see the status of the server as well. If you happen to pause or delete your server from your hosting provider, it will not delete it from RunCloud, and RunCloud will attempt to still connect with the server, but if it unable to do so, you will be prompted with further action.
