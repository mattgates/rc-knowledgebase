# Overview of the RunCloud Dashboard


The RunCloud dashboard is feature-rich and full of useful information about your server and instead of using SSH to connect with your server, you can do just about everything you need to do to any web application right from RunCloud. RunCloud’s dashboard was carefully designed and well thought out for immediate and most useful information. Right when you click into your server, you are presented with a dashboard.

On the 5-day trial mode, you will have full access to all features within the dashboard. The free plan comes with limitations, while the basic plan will give you access to nearly everything you need.

### SUMMARY

CPU: this is the amount of core processing power that your current server has.

MEMORY USAGE: As you add more websites or web apps to your server, all of which are doing some type of work, whether they are just a blog or an actual application performing a series of processing tasks, this number will likely increase, as a website is really just software that is using resources on a computer. The higher this number, the slower your website is likely to be, as the amount of resources available overall is less.

With nothing installed, RunCloud uses around 200 MB of memory at its most basic function, with nothing else running.

DISK USAGE: Every server comes with a certain amount of storage space. As you add websites, databases, scripts, libraries, and images, your disk space will be reduced until you max out the amount of storage space allotted to you.

RunCloud uses about 5 GB of storage space after installation.

UPTIME: Unless you manually restarted the server, this tells you the amount of days your server has been up and running. For Linux machines, it is rare that a restart is required. Sometimes a machine will slow down over time or a script will run too long and slow down the entire machine, requiring a restart, but it is unlikely you will have to restart your server. I like to think of UPTIME as “days without incident”.

WEB APPLICATION (TOTAL): The amount of web apps created on the server.

DATABASE (TOTAL): The amount of databases that have been created on the server.

SUPERVISORD: create and monitor specific tasks that need to be run on a daily basis. This feature is always running and never stops, unlike a cronjob.

CRON JOB: The amount of cron jobs currently set to run.

MAP OF SERVER: Rarely does a service ever show you where your current server is located, but RunCloud has pinpointed the exact location from the IP address, showing the location via map of where your server currently resides.
