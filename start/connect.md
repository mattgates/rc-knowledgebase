# Connecting A Server To RunCloud

## Setting up a Vultr instance on RunCloud

I continue to choose Vultr as my hosting provider for the several following reasons:

* Reliability - I have been using them for over 3 years and have never had any issues
* Value - They have the best pricing and deals
* Quality Support - They are quick to respond within 2–4 hours about any technical issues
* Speed - Research has shown they have the fastest servers, specifically in Chicago

With regards to their deals, they are currently running a special offer (at time of this writing) where they match your credit amount — meaning if you put $100 in, they will match it with another $100 — if you can put $500 in, well worth it if you plan to host for a long time — you get a  free $500. 

Vultr is the most affordable with regards to development, every account being allowed to purchase two sandbox servers which can be used to host a few low-traffic websites — for just  $3.50 a month.

My purpose of doing this is to show you just how easy it is to setup a server on Vultr and install RunCloud. Why install RunCloud? It is like an advanced CPanel or Control Panel that helps you monitor your website and helps you set up other websites with ease. More importantly, it gets you off shared hosting and gives you the ability to manage your own server.

Let’s get started!

Head to Vultr.com and register with a new account. Don’t forget to check the coupons section to ensure you’re getting the best deal! After you’ve logged in and entered in all the necessary account information you will be able to provision a server, click the big circle-plus button on the right to begin.

After having done tons of research, Chicago seems to be the best server. Also, since the coasts are going to be underwater in about hundred years, it is best to choose the middle of the country. I’m kidding! Los Angeles, Atlanta, and New Jersey are just fine as well.

Next, choose the type of software you want installed on your VPS, or virtual private server. This is important as RunCloud only allows Ubuntu. Choose Ubuntu 18.04 as newer LTS versions have a longer support window and ensure stability. 

Next comes the pricing! For this sample and for your wallet — and mine, we’re going to go with the cheapest plan that Vultr offers. $3.50 per month! Pretty cheap. But powerful and fast since your website is the only one on this virtual computer. If you want to pay $5 per month, you can actually install WordPress right on your server, but Vultr will not install any additional optimization software or security software on your server. RunCloud is the software that takes care of all the updates, optimizations, and security for your server. 

If you proceed with the route of installing WordPress, you will not be able to use the $3.50 per month plan. Vultr does not allow it for multiple reasons including their own setup — and of course, they don’t really make money on those plans, so getting you to upgrade is more cost-effective. However, 512 MB of memory is more than enough for a simple WordPress website, such as a blog. So continue with the cheapest plan to save the most money.

The last thing we’ll do is give your server a name. Anything you want. In this example, it’s just runcloud.

Finally, hit that Deploy Now button and you’ve just created your server.

Server added successfully!

After, you will need to allow RunCloud to be installed on your server. Navigate to the Firewall section of Vultr and open ports 22, 80, 443, and 34210.

It will take a few minutes for Vultr to set up the server. As you are waiting, head over to RunCloud and register for an account. You can start on the free account. Once you’ve logged in, get to the Connect Server screen.

Head back to Vultr and you will probably see your new server already setup. Click into your new server.

You will land at the dashboard. Grab the IP address:

Head back to RunCloud where you will now see this screen to input the name of the server, the IP address, and optionally, the server provider. I always fill this out as it might help RunCloud staff understand where you’re coming from if you ever need technical support. Click on ‘Connect this server’.

The next screen gives you a code command that you will need to copy and execute on your server as the root user.

Here is where the hardest part you will ever face comes in. I spoke with RunCloud’s founder who says they are working on automatically connecting to the server instead of having to put this code in manually. 

At the time of this writing, it is not possible. Before you copy this code, you can use SSH via Mac OS Terminal, Linux Terminal, or use WinSCP for SSH. If you are confused about that language, here is a Chrome extension you can download: https://chrome.google.com/webstore/detail/secure-shell-app/pnhechapfaindjhompbnflcldabbghjo?hl=en

You will be logging in by typing in root@ipaddress and hitting enter. 

You will be prompted with a “fingerprint”, for which you will want to answer Yes, and for the password. Copy the password from the previous Vultr server screen and paste it right into the terminal, and hit enter. It will not display the actual password for security reasons.

Once you’ve gotten through this, you will see a command prompt. It is time to enter the code provided by RunCloud in the previous step. Paste this code into Terminal and hit enter to execute it as root user.

The software installation takes about 5–10 minutes. Be patient. There will be some pauses, but everything will work. You will see status updates as everything downloads and installs itself automatically. One of the great things about RunCloud is that it automatically installs everything you need, especially MariaDB which is supposedly faster and more efficient than MySQL database. Read this amazing review of MySQL vs. MariaDB for additional information.

By the time you are done reading all of this, it is likely that your RunCloud software has been installed and you should get a screen with a password. Make sure you copy that information and store it somewhere safely on your computer.

Once you return back to RunCloud.io, the dashboard will look like this:

If you see this screen, everything was successful. However, this does not mean your website is ready at all. Upon visiting your server by typing the IP address into the browser, you will still get this screen:

This site can’t be reached
144.202.61.101 refused to connect.
Try:
Checking the connection
Checking the proxy and the firewall
ERR_CONNECTION_REFUSED

Don’t be scared. This is normal because we just installed the RunCloud software service on your server. It is not actually nothing pointing to anything else on your server, such as a website. 